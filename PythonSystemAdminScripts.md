# PythonSystemAdminCommands

# Creating Files With Python
## syntax f = open('filename.txt','w')
##        f = write('Type your text')
##        f.close()
Steps
1. Initiate open function open() and append 'w' for writing
2. Insert your text using the write function write()
3. Closing the function with close()


# Appending Changes to a File
## Syntax f = open('filename.txt','a')
##        f.write('Type extra lines')
##        f.close()
Steps are simmilar to creating and writing a file. 
Change 'w' to 'a'


# Reading Fils With Python
## syntax f = open('fielname','r')
##         print(f.read())


w - create and write
a - append changes
r - read


